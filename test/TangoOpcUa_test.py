#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of the TangoOpcUa project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
"""Contain the tests for the TangoOpcUaObject class."""

# Path
import sys
import os
path = os.path.join(os.path.dirname(__file__), os.pardir)
sys.path.insert(0, os.path.abspath(path))

# Imports
from time import sleep
from mock import MagicMock
from PyTango import DevFailed, DevState
from devicetest import DeviceTestCase, main
from TangoOpcUa import TangoOpcUa

# Note:
#
# Since the device uses an inner thread, it is necessary to
# wait during the tests in order the let the device update itself.
# Hence, the sleep calls have to be secured enough not to produce
# any inconsistent behavior. However, the unittests need to run fast.
# Here, we use a factor 3 between the read period and the sleep calls.
#
# Look at devicetest examples for more advanced testing


# Device test case
class TangoOpcUaDeviceTestCase(DeviceTestCase):
    """Test case for packet generation."""
    # PROTECTED REGION ID(TangoOpcUa.test_additionnal_import) ENABLED START #
    # PROTECTED REGION END #    //  TangoOpcUa.test_additionnal_import
    device = TangoOpcUa
    properties = {'StateDefinition': '', 'OpcTags': '', 'OpcUaServerAddress': '', 
                  }
    empty = None  # Should be []

    @classmethod
    def mocking(cls):
        """Mock external libraries."""
        # Example : Mock numpy
        # cls.numpy = TangoOpcUa.numpy = MagicMock()
        # PROTECTED REGION ID(TangoOpcUa.test_mocking) ENABLED START #
        # PROTECTED REGION END #    //  TangoOpcUa.test_mocking

    def test_properties(self):
        # test the properties
        # PROTECTED REGION ID(TangoOpcUa.test_properties) ENABLED START #
        # PROTECTED REGION END #    //  TangoOpcUa.test_properties
        pass

    def test_State(self):
        """Test for State"""
        # PROTECTED REGION ID(TangoOpcUa.test_State) ENABLED START #
        self.device.State()
        # PROTECTED REGION END #    //  TangoOpcUa.test_State

    def test_Status(self):
        """Test for Status"""
        # PROTECTED REGION ID(TangoOpcUa.test_Status) ENABLED START #
        self.device.Status()
        # PROTECTED REGION END #    //  TangoOpcUa.test_Status

    def test_Connect(self):
        """Test for Connect"""
        # PROTECTED REGION ID(TangoOpcUa.test_Connect) ENABLED START #
        self.device.Connect()
        # PROTECTED REGION END #    //  TangoOpcUa.test_Connect

    def test_Disconnect(self):
        """Test for Disconnect"""
        # PROTECTED REGION ID(TangoOpcUa.test_Disconnect) ENABLED START #
        self.device.Disconnect()
        # PROTECTED REGION END #    //  TangoOpcUa.test_Disconnect


# Main execution
if __name__ == "__main__":
    main()

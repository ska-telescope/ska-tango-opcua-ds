## TangoOpcUaObject class

This class aims to representing an OpcUa Object by defining properties.
It must be used with an OpcUaAccess device

## Requirement

- PyTango >= 8.1.6
- freeopcua (an open-source (LGPL) OPC-UA stack and associated tools)
- devicetest (for using tests)
- sphinx (for building sphinx documentation)

Run in terminal, to install requirements:

	pip install -r requirements.txt  

## Installation

Run python setup.py install

If you want to build sphinx documentation,
run python setup.py build_sphinx

If you want to pass the tests, 
run python setup.py test

## Usage

Now you can start your device server in any
Terminal or console by calling it :

TangoOpcUa instance_name

### Device server properities

- OpcUaServerAddress, e.g.: opc.tcp://192.168.64.12:4840
- StateDeﬁnition , e.g.: MyDevice.Value!1:ON!2:OFF!3:ALARM!15:FAULT
- OpcTags, e.g.: Resistance!Scalar!Double!READ_WRITE!DeviceName.Resistor1! DeviceName.Resistor1

### Device server commands:

- Connect
- Disconnect


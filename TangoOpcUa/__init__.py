# -*- coding: utf-8 -*-
#
# This file is part of the TangoOpcUa project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""TangoOpcUaObject class

This class aims to representing an OpcUa Object by defining properties.
It must be used with an OpcUaAccess device
"""

from . import release
from .TangoOpcUa import TangoOpcUa, main

__version__ = release.version
__version_info__ = release.version_info
__author__ = release.author

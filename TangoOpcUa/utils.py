import PyTango
from opcua.ua.uaerrors import BadNoMatch


class OpcItem:
    """
    Configuration item representing a single Tango attribute.
    """
    def __init__(self, str_in):
        str_parse = str_in.split('!')
        self.name = str_parse[0]
        self.x_dim_type = str_parse[1]
        self.value_type = str_parse[2]
        self.access = str_parse[3]
        self.read_tag = str_parse[4]
        if "write" in self.access.lower():
            self.write_tag = str_parse[5]

    def __repr__(self):
        representation = "%s (%s, %s) reading from %s" % (self.name, self.value_type,
                                                          self.access, self.read_tag)
        try:
            representation += ", writing to %s" % self.write_tag
        except AttributeError:
            pass
        return representation


class OpcStateItem:
    """
    Configuration item representing a single Tango state-defining OPC tag.
    """
    def __init__(self, str_in):
        self.state_dict = {}
        str_parse = str_in.split('!')
        self.read_tag = str_parse[0]
        for state_def in str_parse[1:]:
            value, state = state_def.split(":")
            self.state_dict[value] = state.upper()


class SubHandler(object):
    """
    Subscription Handler. To receive events from server for a subscription
    data_change and event methods are called directly from receiving thread.
    Do not do expensive, slow or network operation there. Create another
    thread if you need to do such a thing
    """
    def __init__(self, data_dict, attribute_name):
        self.data_dict = data_dict
        self.attribute_name = attribute_name

    def datachange_notification(self, node, val, data):
        print("New data change event", node, val)
        self.data_dict[self.attribute_name] = data.monitored_item.Value.Value.Value


def node_search(parent, browse_name):
    """
    Search a parent node's children for a specific browse name and return that node
    """
    _objects_children = parent.get_children()
    for node in _objects_children:
        _child_name = node.get_browse_name()
        if _child_name.Name == browse_name:
            return node
    return None


def find_node_through_hierarchy(objects_node, hierarchy):
    parent = objects_node
    for hierarchy_item in hierarchy:
        try:
            parent = node_search(parent, hierarchy_item)
        except BadNoMatch:
            return None
    return parent


DICT_TANGO_TYPE = {'Bool': PyTango.DevBoolean,
                   'UShort': PyTango.DevUShort,
                   'Short': PyTango.DevShort,
                   'ULong': PyTango.DevULong,
                   'Long': PyTango.DevLong,
                   'ULong64': PyTango.DevULong64,
                   'Long64': PyTango.DevLong64,
                   'Double': PyTango.DevDouble,
                   'String': PyTango.DevString,
                   'Float': PyTango.DevFloat}
DICT_TANGO_ACCESS = {'read': PyTango.READ,
                     'write': PyTango.WRITE,
                     'read_write': PyTango.READ_WRITE}
DICT_TANGO_STATE = {'ON': PyTango.DevState.ON,
                    'OFF': PyTango.DevState.OFF,
                    'CLOSE': PyTango.DevState.CLOSE,
                    'OPEN': PyTango.DevState.OPEN,
                    'INSERT': PyTango.DevState.INSERT,
                    'EXTRACT': PyTango.DevState.EXTRACT,
                    'MOVING': PyTango.DevState.MOVING,
                    'ALARM': PyTango.DevState.ALARM,
                    'FAULT': PyTango.DevState.FAULT,
                    'INIT': PyTango.DevState.INIT,
                    'RUNNING': PyTango.DevState.RUNNING,
                    'STANDBY': PyTango.DevState.STANDBY,
                    'DISABLE': PyTango.DevState.DISABLE,
                    'UNKNOWN': PyTango.DevState.UNKNOWN}
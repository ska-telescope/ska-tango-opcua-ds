# -*- coding: utf-8 -*-
#
# This file is part of the TangoOpcUa project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""Release information for Python Package"""

name = """tangods-tangoopcua"""
version = "0.5.0"
version_info = version.split(".")
description = """This class aims to representing an OpcUa Object by defining properties.
It must be used with an OpcUaAccess device"""
author = "lukasz.j.dudek"
author_email = "lukasz.j.dudek at uj.edu.pl"
license = """GPL"""
url = """www.tango-controls.org"""
copyright = """"""

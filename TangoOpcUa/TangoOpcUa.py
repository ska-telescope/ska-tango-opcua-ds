# -*- coding: utf-8 -*-
#
# This file is part of the TangoOpcUa project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" TangoOpcUaObject class

This class aims to representing an OpcUa Object by defining properties.
It must be used with an OpcUaAccess device
"""

__all__ = ["TangoOpcUa", "main"]

# PyTango imports
import PyTango
from PyTango import DebugIt
from PyTango.server import run
from PyTango.server import Device, DeviceMeta
from PyTango.server import attribute, command
from PyTango.server import class_property, device_property
from PyTango import AttrQuality, AttrWriteType, DispLevel, DevState
# Additional import
# PROTECTED REGION ID(TangoOpcUa.additionnal_import) ENABLED START #
import sys
import os
from opcua.client.client import Client
try:
    from TangoOpcUa.TangoOpcUa.utils import OpcStateItem, OpcItem, DICT_TANGO_TYPE,\
        DICT_TANGO_ACCESS, DICT_TANGO_STATE, find_node_through_hierarchy, SubHandler
except ImportError:
    sys.path.append(os.getcwd())
    from utils import OpcStateItem, OpcItem, DICT_TANGO_TYPE, DICT_TANGO_ACCESS,\
        DICT_TANGO_STATE, find_node_through_hierarchy, SubHandler
# PROTECTED REGION END #    //  TangoOpcUa.additionnal_import


class TangoOpcUa(Device):
    """
    This class aims to representing an OpcUa Object by defining properties.
    It must be used with an OpcUaAccess device
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(TangoOpcUa.class_variable) ENABLED START #
    SUBSCRIPTION_TIME = 100
    opcua_client = None
    configuration_list = None
    state_definition = None
    handlers = []
    subscriptions = []
    data = {}

    @staticmethod
    def parse_conf(configuration):
        conf_list = []
        for str_item in configuration:
            l_item = OpcItem(str_item)
            conf_list.append(l_item)
        return conf_list

    def prepare_subscription(self, tag, attribute_name):
        self.debug_stream("In prepare_subscription(%s): got tag %s" % (attribute_name, tag))
        self.data[attribute_name] = None
        node_names = tag.split('.')
        objects_node = self.opcua_client.get_objects_node()
        value_node = find_node_through_hierarchy(objects_node, node_names)
        if value_node is None:
            self.error_stream("Cannot find value node under %s for attribute %s" % (node_names, attribute_name))
        else:
            handler = SubHandler(self.data, attribute_name)
            self.handlers.append(handler)
            subscription = self.opcua_client.create_subscription(self.SUBSCRIPTION_TIME, handler)
            self.subscriptions.append(subscription)
            handle = subscription.subscribe_data_change(value_node)

    def dev_state(self):
        self.debug_stream("In dev_state()")
        if self.state_definition:
            tag_value = self.data["DevState"]
            self.set_status("Custom status because of %s value: %s" % (self.state_definition.read_tag,
                                                                       tag_value))
            new_state = "UNKNOWN"
            for value, state in self.state_definition.state_dict.items():
                if str(tag_value) == value:
                    new_state = state
            new_tango_state = DICT_TANGO_STATE[new_state]
            return new_tango_state
        else:
            self.set_status("StateDefinition property not defined.")
            return PyTango.DevState.UNKNOWN

    def initialize_dynamic_attributes(self):
        self.debug_stream("In initialize_dynamic_attributes()")
        for item in self.configuration_list:
            item_type = DICT_TANGO_TYPE[item.value_type]
            item_access = DICT_TANGO_ACCESS[item.access.lower()]
            if item_access == PyTango.READ:
                attr = PyTango.Attr(item.name, item_type, item_access)
                self.add_attribute(attr, TangoOpcUa.read_attr, None, None)
            elif item_access == PyTango.WRITE:
                attr = PyTango.Attr(item.name, item_type, item_access)
                self.add_attribute(attr, None, TangoOpcUa.write_attr, None)
            elif item_access == PyTango.READ_WRITE:
                attr = PyTango.Attr(item.name, item_type, item_access)
                self.add_attribute(attr, TangoOpcUa.read_attr, TangoOpcUa.write_attr, None)
            self.prepare_subscription(item.read_tag, item.name)

    def read_attr(self, attr):
        self.debug_stream("In read_attr(%s)" % attr.get_name())
        value = self.data[attr.get_name()]
        attr.set_value(value)

    def write_attr(self, attr):
        self.debug_stream("In write_attr(%s)" % attr.get_name())
        current_attr_conf = None
        for conf_item in self.configuration_list:
            if conf_item.name == attr.get_name():
                current_attr_conf = conf_item
        objects_node = self.opcua_client.get_objects_node()
        value_node = find_node_through_hierarchy(objects_node, current_attr_conf.write_tag.split('.'))
        write_value = attr.get_write_value()
        value_node.set_value(write_value)
    # PROTECTED REGION END #    //  TangoOpcUa.class_variable
    # ----------------
    # Class Properties
    # ----------------

    # -----------------
    # Device Properties
    # -----------------

    StateDefinition = device_property(
        dtype='str',
    )
    OpcTags = device_property(
        dtype=('str',),
    )
    OpcUaServerAddress = device_property(
        dtype='str',
    )
    # ----------
    # Attributes
    # ----------

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        Device.init_device(self)
        # PROTECTED REGION ID(TangoOpcUa.init_device) ENABLED START #
        self.opcua_client = Client(self.OpcUaServerAddress)
        self.opcua_client.connect()
        self.configuration_list = self.parse_conf(self.OpcTags)
        self.state_definition = OpcStateItem(self.StateDefinition)
        self.prepare_subscription(self.state_definition.read_tag, "DevState")
        self.set_state(self.dev_state())
        # PROTECTED REGION END #    //  TangoOpcUa.init_device

    def always_executed_hook(self):
        # PROTECTED REGION ID(TangoOpcUa.always_executed_hook) ENABLED START #
        pass
        # PROTECTED REGION END #    //  TangoOpcUa.always_executed_hook

    def delete_device(self):
        # PROTECTED REGION ID(TangoOpcUa.delete_device) ENABLED START #
        if self.opcua_client:
            self.opcua_client.disconnect()
        # PROTECTED REGION END #    //  TangoOpcUa.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    # --------
    # Commands
    # --------

    @command
    @DebugIt()
    def Connect(self):
        # PROTECTED REGION ID(TangoOpcUa.Connect) ENABLED START #
        self.opcua_client.connect()
        self.handlers = []
        self.subscriptions = []
        self.prepare_subscription(self.state_definition.read_tag, "DevState")
        for item in self.configuration_list:
            self.prepare_subscription(item.read_tag, item.name)
        # PROTECTED REGION END #    //  TangoOpcUa.Connect

    @command
    @DebugIt()
    def Disconnect(self):
        # PROTECTED REGION ID(TangoOpcUa.Disconnect) ENABLED START #
        self.opcua_client.disconnect()
        self.data["DevState"] = None
        # PROTECTED REGION END #    //  TangoOpcUa.Disconnect

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(TangoOpcUa.main) ENABLED START #
    from PyTango.server import run
    return run((TangoOpcUa,), args=args, **kwargs)
    # PROTECTED REGION END #    //  TangoOpcUa.main

if __name__ == '__main__':
    main()
